*** Settings ***
Library			SeleniumLibrary
Suite Setup		Go to Qwant
Suite Teardown	Close All Browsers


*** Test Cases ***
Search for squashtest.com
	Search and check				squashtest			squashtest
    Follow result link and check						L'outillage de test de la transformation numérique


*** Keywords ***
Go to Qwant
    Set Selenium Speed			1
	Open Browser				https://www.qwant.com/	chrome
    Maximize Browser Window

Search and check
	[Arguments]						${query}							${expected_result}
	wait until element is visible	css=form[data-testid=mainSearchBar]
	Input Text						css=input[name=q]					${query}
	Press Keys						None								ENTER
	Wait Until Page Contains		${expected_result}

Follow result link and check
	[Arguments]																				${expected_result}
	Click Element				xpath:(//div[@data-testid="sectionWeb"]//a)[1]
    Wait Until Page Contains																${expected_result}